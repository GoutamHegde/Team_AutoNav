
#include "stdio.h"
#include "_can_dbc/generated_can.h"

// CAN signal structures
SENSOR_SONAR_t sensor_can_msg = { near_f,near_fr,near_fl,near_b };
MOTOR_CONTROL_t motor_can_msg = { straight,stop };
START_CMD_t bluetooth_start = { 0 };

/* TODO: Shangming: Better to declare function in header file, and create a Motor_Sensor.cpp to define the function */

bool obstacleAvoid (SENSOR_SONAR_t sensor_can_receive)
{

    // Handling obstacle at Near Region

    if ( sensor_can_receive.Sensor_Sonar_Front == near_f || sensor_can_receive.Sensor_Sonar_FrontRight == near_fr || sensor_can_receive.Sensor_Sonar_FrontLeft == near_fl)
    {
            if ( (sensor_can_receive.Sensor_Sonar_Front == near_f && sensor_can_receive.Sensor_Sonar_FrontRight == near_fr && sensor_can_receive.Sensor_Sonar_FrontLeft == near_fl) || (sensor_can_receive.Sensor_Sonar_FrontRight == near_fr && sensor_can_receive.Sensor_Sonar_FrontLeft == near_fl))
            {
                motor_can_msg.speed = stop;
                motor_can_msg.steer = straight;
            }

            else if ( sensor_can_receive.Sensor_Sonar_Front == near_f)
            {
                motor_can_msg.speed = stop;
                motor_can_msg.steer = straight;

            }
/*
            else if ( sensor_can_receive.Sensor_Sonar_Front == near_f && sensor_can_receive.Sensor_Sonar_FrontRight == near_fr )
            {
                motor_can_msg.speed = slow;
                motor_can_msg.steer = left;
            }

            else if ( sensor_can_receive.Sensor_Sonar_Front == near_f && sensor_can_receive.Sensor_Sonar_FrontLeft == near_fl )
            {
                motor_can_msg.speed = slow;
                motor_can_msg.steer = right;
            }
*/

            else if ( sensor_can_receive.Sensor_Sonar_FrontLeft == near_fl)
            {
                motor_can_msg.speed = slow;
                motor_can_msg.steer = right;
            }

            else if ( sensor_can_receive.Sensor_Sonar_FrontRight == near_fr )
            {
                motor_can_msg.speed = slow;
                motor_can_msg.steer = left;
            }

    }



    // Handling obstacle at Mid Region

    else if ( sensor_can_receive.Sensor_Sonar_Front == mid_f || sensor_can_receive.Sensor_Sonar_FrontRight == mid_fr || sensor_can_receive.Sensor_Sonar_FrontLeft == mid_fl)
    {
            if ( (sensor_can_receive.Sensor_Sonar_Front == mid_f && sensor_can_receive.Sensor_Sonar_FrontRight == mid_fr && sensor_can_receive.Sensor_Sonar_FrontLeft == mid_fl) || (sensor_can_receive.Sensor_Sonar_FrontRight == mid_fr && sensor_can_receive.Sensor_Sonar_FrontLeft == mid_fl))
           {
               motor_can_msg.speed = slow;
               motor_can_msg.steer = straight;
           }

            else if ( sensor_can_receive.Sensor_Sonar_Front == mid_f && sensor_can_receive.Sensor_Sonar_FrontRight == mid_fr )
            {
                motor_can_msg.speed = slow;
                motor_can_msg.steer = sleft;
            }

            else if ( sensor_can_receive.Sensor_Sonar_Front == mid_f && sensor_can_receive.Sensor_Sonar_FrontLeft == mid_fl )
            {
                motor_can_msg.speed = slow;
                motor_can_msg.steer = sright;
            }

            else if ( sensor_can_receive.Sensor_Sonar_Front == mid_f )
            {
                motor_can_msg.speed = slow;
                motor_can_msg.steer = sright;
            }

            else if ( sensor_can_receive.Sensor_Sonar_FrontRight == mid_fr )
            {
                motor_can_msg.speed = slow;
                motor_can_msg.steer = sleft;
            }

            else if ( sensor_can_receive.Sensor_Sonar_FrontLeft == mid_fl )
            {
                motor_can_msg.speed = slow;
                motor_can_msg.steer = sright;
            }



    }


    // Handling obstacle at far region

    else if ( sensor_can_receive.Sensor_Sonar_Front == far_f && sensor_can_receive.Sensor_Sonar_FrontRight == far_fr && sensor_can_receive.Sensor_Sonar_FrontLeft == far_fl )
   {
       motor_can_msg.speed = slow;
       motor_can_msg.steer = straight;
   }


    return true;
}
